# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the circlepuzzle.cibersheep package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: circlepuzzle.cibersheep\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-21 05:41+0200\n"
"PO-Revision-Date: 2020-08-21 05:43+0200\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"
"Last-Translator: Joan CiberSheep <cibersheep@gmail.com>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: ca\n"

#: ../About.qml.in:37
msgid "About"
msgstr "Quant a"

#: ../About.qml.in:98
msgid "Version %1. Source %2"
msgstr "Versió %1. Codi font %2"

#: ../About.qml.in:110
msgid "Under License %1"
msgstr "Sota llicència %1"

#: ../About.qml.in:130
msgid "How to play"
msgstr "Com jugar"

#: ../About.qml.in:147
msgid "Rotate the cicles to form four interconnected circles of the same color"
msgstr ""
"Gireu els cercles per formar-ne cuatre d'interconnectats del mateix color"

#: ../About.qml.in:176
msgid "App Development"
msgstr "Desenvolupament de l'app"

#: ../qml/Main.qml:64 circlepuzzle.desktop.in.h:1
msgid "That Circle Puzzle"
msgstr "That Circle Puzzle"

#: ../qml/Main.qml:72
msgid "Information"
msgstr "Informació"

#: ../qml/Main.qml:83
msgid "Shuffle"
msgstr "Mescla"

#: ../qml/components/SuccessDialog.qml:7
msgid "Congratulations"
msgstr "Enhorabona"

#: ../qml/components/SuccessDialog.qml:8
msgid "Well done. You have resolved the puzzle!"
msgstr "Ben fet. Heu resolt el trencaclosques!"

#: ../qml/components/SuccessDialog.qml:11
msgid "Ok"
msgstr "D'acord"
