import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
    id: successDialog
    title: i18n.tr("Congratulations")
    text: i18n.tr("Well done. You have resolved the puzzle!")

    Button {
        text: i18n.tr("Ok")
        onClicked: PopupUtils.close(successDialog)
    }
}

